package com.ariasolutions.util;

public class Check {
  public static <T> T notNull(T v, String name) {
    if(v == null) {
      throw new NullPointerException(name + " is null.");
    }
    return v;
  }
}
