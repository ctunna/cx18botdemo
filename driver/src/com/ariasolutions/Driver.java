package com.ariasolutions;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ariasolutions.util.Check;
import com.ariasolutions.util.StringUtil;
import com.genesyslab.mcr.smserver.channel.ChannelDriver;
import com.genesyslab.mcr.smserver.channel.ChannelDriverConstants;
import com.genesyslab.mcr.smserver.channel.ChannelDriverPU;
import com.genesyslab.mcr.smserver.gframework.FieldNames;
import com.genesyslab.mcr.smserver.gframework.LmsMessages;
import com.genesyslab.platform.commons.collections.KeyValueCollection;
import com.genesyslab.platform.openmedia.protocol.interactionserver.events.EventAck;
import com.genesyslab.platform.openmedia.protocol.interactionserver.requests.interactionmanagement.RequestSubmit;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Driver implements ChannelDriver {
  ChannelDriverPU channel;

  private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
  private Params params;
  private TestMonitor monitor = new TestMonitor(this);
  private boolean connected = false;

  private Map<String, Session> sessions = new HashMap<>();

  @Override
  public String getName() {
    return null;
  }

  @Override
  public String getVersion() {
    return null;
  }

  @Override
  public String getMediaAccount() {
    return null;
  }

  @Override
  public KeyValueCollection getService(String s, String s1, KeyValueCollection keyValueCollection) throws Exception {
    return null;
  }

  @Override
  public void initialize(ChannelDriverPU channelDriverPU) {
    channel = channelDriverPU;
    params = new Params(this);
    params.getConfiguration();
    executor.scheduleWithFixedDelay(monitor, 0, 5, TimeUnit.SECONDS);
    log("Initialized.");
  }

  @Override
  public void shutdown() {
    log("Request to shutdown.");
    disconnect();
  }

  @Override
  public void connect() throws IOException {
    if (!connected) {
      channel.connectionState(true, "connected now");
      connected = true;
      monitor.start();
    }
  }

  @Override
  public void disconnect() {
    if (connected) {
      channel.connectionState(false, "disconnected now");
      connected = false;
      monitor.stop();
    }
  }

  @Override
  public boolean isConnected() {
    return connected;
  }

  @Override
  public void exeModeChanged(String s) {
    log("Exe mode changed.");
  }

  @Override
  public void configurationChanged(KeyValueCollection keyValueCollection, KeyValueCollection keyValueCollection1, KeyValueCollection keyValueCollection2) throws Exception {
    log("Configuration changed.");
  }

  @Override
  public void submitMessageResult(String requestId, boolean success, KeyValueCollection resultData) throws Exception {
    log(LmsMessages.called, String.format("with result:: requestId=%s, success=%b, resultData=...\n%s", requestId, success, resultData));
  }

  @Override
  public void chatSessionNotification(String s, KeyValueCollection keyValueCollection) {
    log("Chat session notification: " + s);
  }

  @Override
  public String umsRequest(String serverRequestId, String serviceName, String methodName, KeyValueCollection requestData) {
    try {
      String result = CompletableFuture
              .completedFuture(1)
              .thenApplyAsync((v) -> {
                try {
                  if (!connected) {
                    log(LmsMessages.error2, String.format("connection with media source was not established, umsRequest id=%s", serverRequestId));
                    channel.umsFaultResponse(serverRequestId, 1101, "connection with media source was not established", null);
                    return serverRequestId;
                  }
                  if ("Session".equals(serviceName) && "SesSendOutbMsg".equals(methodName)) {
                    return outboundMessageHandler(serverRequestId, serviceName, methodName, requestData);
                  }
                  if ("Last".equals(serviceName) && "Update".equals(methodName)) {
                    return submitMessageHandler(serverRequestId, serviceName, methodName, requestData);
                  }
                } catch (Exception e) {
                  log(e);
                }
                String errMsg = String.format("unknown methodName received %s", methodName);
                log(LmsMessages.error2, errMsg);
                channel.umsFaultResponse(serverRequestId, 1102, errMsg, null);
                return serverRequestId;
              }, executor)
              .get();

      log("Ums request: " + result);
      return result;
    } catch (InterruptedException e) {
      log(e.getMessage());
    } catch (ExecutionException e) {
      log(e.getMessage());
    } catch (Exception e) {
      channel.umsFaultResponse(serverRequestId, 1102, "Exception handling UMS response", null);
      log(e.getMessage());
      log("Exception processing UMS request");
    }
    return serverRequestId;
  }

  private void log(Integer type, String message) {
    if (channel != null) {
      StackTraceElement[] s = Thread.currentThread().getStackTrace();
      String callee = s.length != 0 ? s[s.length - 1].getMethodName() : null;
      channel.getGfrPU().trace(type, "(" + channel.getChannelName() + ").(driver).(" + callee + "):", message);
    }
  }

  private void log(String message) {
    log(LmsMessages.generic_trc2, message);
  }

  private void log(Exception e) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    e.printStackTrace(pw);
    String sStackTrace = sw.toString();
    log(e.toString());
    log(sStackTrace);
  }

  private String outboundMessageHandler(String serverRequestId, String serviceName, String methodName, KeyValueCollection requestData) {
    log("Request data: " + requestData.toStringLine());
    String msg = requestData.getString("_umsMsgPlainText");
    log("SERVICE PENDING OUTBOUND MESSAGE: " + msg);

    if (!msg.toLowerCase().contains("starting the bot")) {
      KeyValueCollection ud = requestData.getList("_umsAttachedData");
      String sid = ud.getString("sid");
      Session s = sessions.get(sid);
      s.messages.remove();
      s.pendingRequest = false;
    }
    channel.umsResponse(serverRequestId, requestData);
    return serverRequestId;
  }

  private String submitMessageHandler(String serverRequestId, String serviceName, String methodName, KeyValueCollection requestData) {
    log("SUBMIT ESP REQUEST");
    log(requestData.toStringLine());

    try {
      String sid = requestData.getString("sid");
      sessions.remove(sid);

      RequestSubmit r = RequestSubmit.create();
      r.setQueue("MyEmail");
      r.setParentInteractionId(requestData.getString("IxnId"));
      r.setMediaType("workitem");
      r.setTenantId(1);
      r.setInteractionType("Inbound");
      r.setInteractionSubtype("InboundNew");
      r.setUserData(requestData);

      EventAck m = (EventAck) channel.sendItxRequestAndWait(r, 1000 * 5);
    } catch (Exception e) {
      log(e);
    }

    channel.umsResponse(serverRequestId, requestData);
    return serverRequestId;
  }

  class TestMonitor implements Runnable, HttpHandler {
    private final Driver driver;
    private HttpServer httpServer;

    private String messageState = "";

    public TestMonitor(Driver driver) {
        this.driver = driver;
    }

    public void start() throws IOException {
      this.httpServer = HttpServer.create(new InetSocketAddress(8887), 0);
      this.httpServer.createContext("/messages" , this);
      this.httpServer.setExecutor(executor); // creates a default executor
      this.httpServer.start();
    }

    public void stop() {
      httpServer.stop(0);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
      Headers responseHeaders = httpExchange.getResponseHeaders();
      responseHeaders.set("Content-Type", "text/plain");
      byte[] responseBody = messageState.getBytes();
      httpExchange.sendResponseHeaders(200, responseBody.length);
      try(OutputStream os = httpExchange.getResponseBody()) {
        os.write(responseBody);
      }
    }

    @Override
    public void run() {
      log("Running...");

      try {
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
        DynamoDB dynamoDB = new DynamoDB(client);
        ScanResult result = client.scan(new ScanRequest().withTableName("transcript"));

        for (Map<String, AttributeValue> item : result.getItems().stream().sorted(Comparator.comparing(o -> Integer.valueOf(o.get("time").getN()))).collect(Collectors.toList())) {
          if (!item.containsKey("read")) {
            log("Found row: " + item.toString());

            String sid = item.get("session").getS();
            Session s = sessions.getOrDefault(sid, new Session());
            s.messages.add(item);
            sessions.put(sid, s);

            Table table = dynamoDB.getTable("transcript");
            UpdateItemSpec spec = new UpdateItemSpec()
                    .withPrimaryKey("responseId", item.get("responseId").getS())
                    .withUpdateExpression("set #na = :val1")
                    .withNameMap(new NameMap().with("#na", "read"))
                    .withValueMap(new ValueMap().withInt(":val1", 1))
                    .withReturnValues(ReturnValue.ALL_NEW);

            table.updateItem(spec);
          }
        }

        StringBuilder sb = new StringBuilder();
        for(Session s : sessions.values().stream().filter(v -> v.initialized).collect(Collectors.toList())) {
          if(!s.messages.isEmpty()) {
            Map<String, AttributeValue> item = s.messages.peek();
            sb.append(StringUtil.join(item.entrySet().stream().sorted(Comparator.comparing(o -> o.getKey())).map(v -> v.getValue().getS()).filter(v -> v != null).collect(Collectors.toList()), ";")).append("\n");
          }
        }
        messageState = sb.toString();

        for (Map.Entry<String, Session> item : sessions.entrySet()) {
          String sid = item.getKey();
          Session s = item.getValue();
          if (s.initialized) {
            if (!s.messages.isEmpty()) {
              Map<String, AttributeValue> record = s.messages.peek();
              if (!s.pendingRequest) {
                String rid = record.get("responseId").getS();
                String query = record.get("queryResult.queryText").getS();
                String resp = record.get("queryResult.fulfillmentText").getS();
                log("Sending: " + query);
                s.pendingRequest = true;
                send(item.getKey(), "Santa", "Timmy", query, resp, rid);
              }
            }
          } else {
            send(sid, "Santa", "Timmy", "Hello", "X", String.valueOf(new Random().nextLong()));
            s.initialized = true;
          }
        }
      } catch (Exception e) {
        log(e);
      }
    }

    private void send(String sid, String from, String to, String message, String resp, String rid) {
      try {
        KeyValueCollection messageData = new KeyValueCollection();

        driver.log("Building message data");

        // SM Server's required fields =====================================================
        messageData.addString(FieldNames.UMS_Channel, channel.getChannelName());
        messageData.addString(FieldNames.UMS_MsgContext, from + to);

        driver.log("Populating MessageSid fields");
        messageData.addString(ChannelDriverConstants.UMSKEY_MsgId, rid);

        driver.log("Populating From fields");
        messageData.addString(ChannelDriverConstants.UMSKEY_FromAddr, from);
        messageData.addString(ChannelDriverConstants.UMSKEY_PhoneNumber, from);
        messageData.addString(FieldNames.UMS_FromAddr, from);
        messageData.addString("_smsSrcNumber", from);


        driver.log("Populating To fields");
        messageData.addString(ChannelDriverConstants.UMSKEY_ToAddr, to);
        messageData.addString(FieldNames.UMS_ToAddr, to);
        messageData.addString("_smsText", to);

        messageData.addString("_OutboundToAddress", "qa@blueksy.com");

        driver.log("Populating Body fields");
        messageData.addUTFString(ChannelDriverConstants.UMSKEY_MsgPlainText, message);
        messageData.addString(FieldNames.UMS_MsgPlainText, message);

//            messageData.addString(FieldNames.UMS_MediaType, "sms");
//            messageData.addString(FieldNames.UMS_MediaTypeChat, "smssession");
        messageData.addString(FieldNames.UMS_MediaType, "pagesample");
        messageData.addString(FieldNames.UMS_MediaTypeChat, "sample");
        messageData.addString(FieldNames.UMS_MsgType, "regular");
        messageData.addString(FieldNames.UMS_ChatPossible, "true");
        messageData.addString(FieldNames.UMS_ChatRequired, "true");

        messageData.addString("sid", sid);
        messageData.addString("rid", rid);
        messageData.addString("myResponse", resp);

        messageData.addString("ChatBotID", "EchoBot");
        messageData.addString("ChatBotName", "EchoBot");
        messageData.addString("ChatBotHoldup", "true");
        messageData.addString("StopBotOnAgentArrival", "false");
        messageData.addString("StopBotOnCustomerLeft", "false");
        messageData.addString("Visibility", "ALL");
        messageData.addString("Nickname", "Caitlin the Voice Bot");

        messageData.addString("Subject", "Booking a flight");
        messageData.addString("Departure Flight", "Orlando to Vegas 8AM");
        messageData.addString("Return Flight", "Vegas to Orlando 5PM");
        messageData.addString("Total Cost", "$360.21");
        messageData.addString("Credit Card", "XXXX XXXX XXXX 6219");

        // =================================================================================

        driver.log("data from sample...\n" + messageData.toStringLine());

        // Submit message ============================================
        channel.submitMessage(sid, messageData);
        // ===========================================================
      } catch (Exception exc) {
        driver.log("Exception processing Twiml: " + exc.getMessage());
      }
    }
  }

  class Session {
    public Queue<Map<String, AttributeValue>> messages = new LinkedList<>();
    public boolean pendingRequest = false;
    public boolean initialized = false;
  }
}
