package com.ariasolutions;

import com.genesyslab.mcr.smserver.gframework.CfgOptions;

import java.util.ArrayList;
import java.util.Properties;

public class Params {
  protected String inboundMedia;
  protected String itxType = "Inbound";
  protected String itxSubType = "InboundNew";

  protected int samplingPeriod;
  protected int messagesCreateMax;
  protected int itxSubmitTimeout;
  protected int itxResubmitDelay;
  protected int itxResubmitAttempts;

  /**
   * The next data field is not used anywhere in the driver, the only purpose of the field is to demonstrate how to
   * get configuration option's values.
   */
  protected Properties queriesList = null;

  private final static String XOPTION_X_INBOUND_MEDIA = "x-inbound-media";
  private final static String MOPTION_SAMPLING_PERIOD = "sampling-period";
  private final static String MOPTION_MESSAGES_CREATE_MAX = "messages-create-max";
  private final static String MOPTION_ITX_SUBMIT_TIMEOUT = "itx-submit-timeout";
  private final static String MOPTION_ITX_RESUBMIT_DELAY = "itx-resubmit-delay";
  private final static String MOPTION_ITX_RESUBMIT_ATTEMPTS = "itx-resubmit_attempts";

  private Driver driver;

  public Params(Driver driver) {
    this.driver = driver;
  }

  protected void getConfiguration() {
    // get channel option XOPTION_x_inbound_media
    inboundMedia = CfgOptions.getOption(
            driver.channel.getChannelName(),
            XOPTION_X_INBOUND_MEDIA,
            null,
            "sample",
            null,
            "",
            true);

    queriesList = getMonitorParams(driver.channel.getChannelName());
  }

  private Properties getMonitorParams(String channelName) {
    Properties twtQueriesProps = new Properties();

    try {
      String monitorSection = channelName + "-monitor";

      // get data fetching monitor's option MOPTION_sampling_period
      samplingPeriod = CfgOptions.getIntOption(monitorSection, MOPTION_SAMPLING_PERIOD, new int[]{60, -3600},
              600, null, "", false);

      // get data fetching monitor's option MOPTION_messages_create_max
      messagesCreateMax = CfgOptions.getIntOption(monitorSection, MOPTION_MESSAGES_CREATE_MAX,
              new int[]{0, -1000000}, 0, null, "", false);

      // get data fetching monitor's option MOPTION_itx_request_timeout
      itxSubmitTimeout = CfgOptions.getIntOption(monitorSection, MOPTION_ITX_SUBMIT_TIMEOUT, new int[]{1, -60},
              30, null, "", false);

      // get data fetching monitor's option MOPTION_itx_repeat_timeout
      itxResubmitDelay = CfgOptions.getIntOption(monitorSection, MOPTION_ITX_RESUBMIT_DELAY,
              new int[]{1, -120}, 30, null, "", false);

      // get data fetching monitor's option MOPTION_itx_attempts_max
      itxResubmitAttempts = CfgOptions.getIntOption(monitorSection, MOPTION_ITX_RESUBMIT_ATTEMPTS,
              new int[]{0, -9}, 3, null, "", false);

      // ......................................................................................
      // get data fetching monitor's options with prefix "qry-" (data frtching queries)
      ArrayList<String> qryNames = CfgOptions.getOptionsNames(monitorSection, "qry-");

      if (null == qryNames)
        return twtQueriesProps;

      for (String qryName : qryNames) {
        String qryValue = CfgOptions.getOption(monitorSection, qryName, null, null, null, "", true);
        twtQueriesProps.setProperty(qryName, qryValue);
      }

      return twtQueriesProps;
    } catch (Exception ex) {
      return null;
    }
  }
}
