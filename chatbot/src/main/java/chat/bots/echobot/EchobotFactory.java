package chat.bots.echobot;

import com.genesyslab.chat.bots.chatbotapi.ChatBot;
import com.genesyslab.chat.bots.chatbotapi.ChatBotFactory;
import com.genesyslab.chat.bots.chatbotapi.platform.ChatInteractionInfo;
import com.genesyslab.chat.bots.chatbotapi.platform.KeyValueMap;
import com.genesyslab.chat.bots.chatbotapi.platform.BotCreationAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchobotFactory implements ChatBotFactory {
	private static final Logger LOG = LoggerFactory.getLogger(EchobotFactory.class);

	public void initialize(KeyValueMap configuration) {
		// TODO to be implemented
	}

	public void configurationUpdated(KeyValueMap configuration) {
		// TODO to be implemented
	}

	public void shutdown() {
		// TODO to be implemented
	}

	public ChatBot createChatBot(KeyValueMap espParameters, ChatInteractionInfo interactionInfo, BotCreationAttributes botCreationAttributes) {
		return new Echobot();
	}

	@Override
	public String getBotId() {
		return "EchoBot";
	}
}
