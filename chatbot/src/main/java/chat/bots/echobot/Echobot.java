package chat.bots.echobot;

import com.genesyslab.chat.bots.chatbotapi.ChatBot;
import com.genesyslab.chat.bots.chatbotapi.chat.ChatEventInfo;
import com.genesyslab.chat.bots.chatbotapi.chat.Enums;
import com.genesyslab.chat.bots.chatbotapi.platform.ChatUserInfo;
import com.genesyslab.chat.bots.chatbotapi.platform.KeyValueMap;
import com.genesyslab.chat.bots.chatbotapi.platform.ChatBotPlatform;
import com.genesyslab.chat.bots.chatbotapi.platform.GenesysChatSession;

import org.slf4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class Echobot implements ChatBot {
  private ChatBotPlatform cbpInstance;
  private Logger logger;
  private GenesysChatSession session;

  @Override
  public void setCbpInstance(ChatBotPlatform cbpInstance) {
    this.cbpInstance = cbpInstance;
    this.logger = cbpInstance.getLogger();
  }

  @Override
  public void onCommandStart(GenesysChatSession session, int eventJoinedId, KeyValueMap espParameters) {
    this.session = session;
    this.logger.info("Bot started.");
  }

  @Override
  public void onCommandStop(StopReason reason, ChatEventInfo eventInfo, KeyValueMap espParameters) {
  }

  @Override
  public void onSessionActivity(ChatEventInfo eventInfo) {
    logger.info("Message text: " + eventInfo.getMessageText());
    ChatUserInfo originator = this.session.getParticipant(eventInfo.getUserId());
    if ((eventInfo.getEventType() == Enums.EventType.MESSAGE) && (originator.getUserType() != Enums.UserType.SYSTEM) && (originator.getUserType() != Enums.UserType.EXTERNAL)) {
      if (eventInfo.getMessageText().matches("stop(:(force_close|keep_alive|close_if_no_agents))?")) {
        Enums.Action action = Enums.Action.CLOSE_IF_NO_AGENTS;
        String[] tokens = eventInfo.getMessageText().split(":");
        if (tokens.length > 1) {
          action = Enums.Action.valueOf(tokens[1].toUpperCase());
        }

        this.logger.info("Leaving chat session with after-action: " + action.name());
        this.cbpInstance.leaveSession(action);
      } else {
        KeyValueMap ud = session.getInteractionInfo().getUserData();
        String sid = ud.get("sid").getAsString();

        if (ud.containsKey("InteractionId")) {
          ud.put("IxnId", ud.get("InteractionId").getAsString());
          this.cbpInstance.updateUserdata(ud);
        }

        this.logger.info("Session id: " + sid);
        String message = sid == null ? null : getResponse(sid);

        if ("Hello".equals(eventInfo.getMessageText()) && message == null) {
          return;
        } else if (message.contains("OK. Bye!") || message == null) {
          Enums.Action action = Enums.Action.CLOSE_IF_NO_AGENTS;
          this.cbpInstance.leaveSession(action);
        } else {
          this.cbpInstance.sendMessage(message);
          this.logger.info("Echoing back message: " + message);
        }
      }
    }
  }

  @Override
  public void onCommandUpdate(KeyValueMap espParameters) {
  }

  String getResponse(String sid)  {
    try {
      HttpURLConnection con = (HttpURLConnection) new URL("http://botgateway.ariasolutions.com:8887/messages").openConnection();
      for (String line : getLines(con.getInputStream())) {
        logger.info("Line: " + line);
        String[] parts = line.split(";");
        if (sid.equals(parts[3])) {
          return parts[0];
        }
      }
    } catch (Exception e) {
      logger.error("Exception thrown getting response: " + e);
    }
    return null;
  }

  static List<String> getLines(InputStream stream) throws IOException {
    try(BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
      return reader.lines().collect(Collectors.toList());
    }
  }
}
