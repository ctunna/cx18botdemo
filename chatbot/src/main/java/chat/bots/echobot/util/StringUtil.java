package chat.bots.echobot.util;

import java.util.Iterator;

public class StringUtil {
  public static String join(Iterable<String> iterable, String separator)
  {
    final StringBuilder sb = new StringBuilder();
    final Iterator<String> it = iterable.iterator();

    if (it.hasNext())
      sb.append(it.next());

    while (it.hasNext())
    {
      sb.append(separator);
      sb.append(it.next());
    }

    return sb.toString();
  }
}