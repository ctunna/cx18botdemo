package chat.bots.echobot.util;

import com.genesyslab.chat.bots.chatbotapi.platform.KeyValueMap;

import java.util.ArrayList;
import java.util.List;

import static chat.bots.echobot.util.StringUtil.join;

public class KeyValueMapUtil {
  static String toString(KeyValueMap map) {
    StringBuilder sb = new StringBuilder("{");
    List<String> pairs = new ArrayList();
    for (String key : map.keySet()) {
      KeyValueMap.ValueHolder h = map.get(key);
      KeyValueMap.ValueType t = h.getValueType();
      switch (t) {
        case BINARY:
          pairs.add(key + ":<binary>");
          break;
        case INT:
          pairs.add(key + ":" + String.valueOf(h.getAsInt()));
          break;
        case LONG:
          pairs.add(key + ":" + String.valueOf(h.getAsLong()));
          break;
        case DOUBLE:
          pairs.add(key + ":" + String.valueOf(h.getAsDouble()));
          break;
        case STRING:
          pairs.add(key + ":" + h.getAsString());
          break;
        case KV_MAP:
          pairs.add(key + ":" + toString(h.getAsKeyValueMap()));
          break;
        default:
      }
    }
    return sb.append(join(pairs, ", ")).append("}").toString();
  }
}
